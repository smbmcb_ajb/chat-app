<?php
include("dbconnection.php");
include("links.php");
include("functions.php");
$userNameErr = $passwordErr = "";
$errorPassword = "";
$uname = "";

    
       // if ($errorPassword==true) {
         //   $passwordErr = "* Password don't match";
        //}
    

    if (isset($_POST['sign-up'])) {
    
        $uname = mysqli_real_escape_string($conn,$_POST['uname']);
        $password = mysqli_real_escape_string($conn,$_POST['password']);
        $passwordRepeat = mysqli_real_escape_string($conn,$_POST['passwordRepeat']);
        if (userNameExist($conn, $uname) !==false) {
            $userNameErr = $uname . " * ...This username is already taken.";
            $added = false;
            } 
        else   
            if (empty($uname)){
                $userNameErr = "* Username is required";
                if (empty($password)) {
                    $passwordErr = "* Password is required";
                
                    if (passwordMatch($passwordRepeat, $password) !== true) {
                        $passwordErr = "* Password don't match";
                        $errorPassword = true;
                        //exit(header("location:sign-up.php?error=passwordMismatch"));
                    }
                    else
                        {
                            $errorPassword = false;
                        }
                }
            
            } elseif (count(explode(" ",$uname))>1) {
                $userNameErr = "* Please do not include white space in username";
                if (empty($password)) {
                    $passwordErr = "* Password is required";
                
                    if (passwordMatch($passwordRepeat, $password) !== true) {
                        $passwordErr = "* Password don't match";
                        $errorPassword = true;
                        //exit(header("location:sign-up.php?error=passwordMismatch"));
                    }
                    else
                        {
                            $errorPassword = false;
                        }
                } elseif (passwordMatch($passwordRepeat, $password) !== true) {
                    $passwordErr = "* Password don't match";
                    $errorPassword = true;
                    //exit(header("location:sign-up.php?error=passwordMismatch"));
                }
            }
            
            elseif (empty($password)) {
                    $passwordErr = "* Password is required";
                    if (passwordMatch($passwordRepeat, $password) !== true) {
                        $passwordErr = "* Password don't match";
                        $errorPassword = true;
                    }
                    else {
                    $errorPassword = false;
                    }
                } elseif (passwordMatch($passwordRepeat, $password) !== true) {
                    $passwordErr = "* Password don't match";
                    $errorPassword = true;
                    
                }
                
            elseif (userNameExist($conn, $uname) !==false) {
                    $userNameErr = $uname . " * ...This username is already taken.";
                    $added = false;
                    }
                    
                else {
                    
                        addUser($conn, $uname, $password);
                        
                        //header("location:sign-up.php?newUserAdded=success");
                        }    
                    
    }
   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        window.addEventListener("load", () =>document.getElementById('name').focus)
    </script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css" />
    <title>Sign-up</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12 mt-4" id="shadow" style="min-width:350px; max-width:480px">
                <div class="mt-4 mb-4">
                    <form action="" method="POST" class="" >
                        <h1 class="text-center">Sign-up</h1>
                        <label class="mt-4 mb-0">Username: </label><div class="text-danger"><small><?php echo "$userNameErr" ?></small></div>
                        <input type="text" class="form-control" id="name" name="uname" value=<?php echo $uname ?>></input>
                        <label class="mb-0 mt-2">Password: </label><div class="text-danger"><small><?php echo "$passwordErr" ?></small></div>
                        <input type="password" class="form-control mt-1" name="password"  />
                        <label class="mb-0 mt-2">Repeat Password: </label><div class="text-danger"><small><?php echo "$passwordErr" ?></small></div>
                        <input type="password" class="form-control mt-1" name="passwordRepeat"  />

                        <div class="mt-4">
                            <button class="btn btn-primary w-100" name="sign-up" id="sign-up">Sign-up</button>
                        </div>
                    </form>
                    <p class="text-center mb-0">Already have an account? <a href="log-in.php">Log-in here</a></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
        </div>
    </div>
</body>
</html>