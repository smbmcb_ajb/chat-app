<?php
session_start();
include("links.php");

if (isset($_SESSION['username'])) {
    $uname = $_SESSION['username'];
}
else 
    header("location:log-in.php");






?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="main.css" />
    
    <script>
        var btn = document.getElementById("search")
    var resultArea = document.getElementById("resultarea")
    window.addEventListener("load", function(){
    btn.focus()
    
    var myRequest = new XMLHttpRequest()
    myRequest.open('POST', 'searchuser.php?key='+btn.value)
    
    myRequest.onload = function() {
        var myData = myRequest.responseText;
        resultArea.innerHTML = myData;
    }
    myRequest.send()

    setInterval(function(){
        var myRequest = new XMLHttpRequest()
    myRequest.open('POST', 'searchuser.php?key='+btn.value)
    
    myRequest.onload = function() {
        var myData = myRequest.responseText;
        resultArea.innerHTML = myData;
    }
    myRequest.send()
    },1000)
})

    </script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat Room</title>
</head>
<body>
<div class="container-fluid mt-4">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12" id="shadow" style="min-width:350px; max-width:480px">
                <form method="POST" action="">
                    <nav class="navbar">
                        <H3 class="text-left">Hello <?php echo $uname; ?>!</H3>
                        <a class="btn btn-danger btn-sm text-right" href="log-out.php" id="log-out">Log-out</a>
                    
                    </nav>
                    <input type="search" placeholder="search.." class="form-control" name="search" id="search" />
                   <h4><p class="mt-4">List of users: </p></h4>
                    <div class="container test2-color">
                        <div>
                            <form action="" method="POST">
                                <ol>
                                    <div id="resultarea"></div>
                                    <script src="main.js">
    
                                    </script>   
                                </ol>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
        <div class="col-md-4 col-sm-4 col-xs-12"></div>
    </div>
    </div>     
<div class="mt-4">

    
</div>

</body>
</html>