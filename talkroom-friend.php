<?php
session_start();
include("links.php");
include("functions.php");
include("dbconnection.php");

if (isset($_SESSION['username'])) {
    $friend = $_GET['friend'];
    $_SESSION['friend'] = $friend;
    $toUser = $friend;
    $fromUser = $_SESSION['username'];
    
    
}
else {
    header("location:log-in.php");
}

if (isset($_POST['send'])) {
    $fromUser = $_SESSION['username'];
    $toUser = $friend;
    $message = $_POST['sendtextarea'];
    
}
    
    $toUser = $friend;


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="main.css" />
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <script>

        var chatArea = document.getElementById("chatarea")
         //var sendMessage = document.getElementById("send")
                            
        window.addEventListener("load", function(){
            var myRequest = new XMLHttpRequest()
                            myRequest.open('POST', 'conversation.php')
                            
                            myRequest.onload = function() {
                                var myData = myRequest.responseText;
                                chatArea.innerHTML = myData;
                                chatArea.scrollIntoView(false)
                            }
                            myRequest.send()
                            
        
        })
        
    </script>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chatbox</title>
</head>
<body>
<div class="container-fluid mt-1">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12" id="shadow" style="min-width:350px; max-width:480px">
                <form method="POST" action="">
                    <nav class="navbar">
                    <a href="index.php" style="text-decoration:none"><i class="fa fa-arrow-left text-left" aria-hidden="true" fa-2xl></i>Back</a>
                        <H4 class="text-right">Send messsage to:  <?php echo $friend; ?>!</H4>
                        
                    </nav>
                    
                   
                    <div class="container test2-color ">
                        <div class="mt-4" style="overflow-y: scroll; max-height:350px; min-height:350px">
                        
                            <div id="chatarea"></div>

                            
                            
                            
                        </div>
                    </div>
                    <div class="input-group mb-3 mt-3">
                        <form action="#" method="POST">
                        <textarea placeholder="type your message here" class="form-control rows=3" name="sendtextarea" id="sendtextarea"></textarea>
                        <button class="btn btn-success" name="send" id="send">Send</button>
                        
                        </form>
                        <script>
                            var chatArea = document.getElementById("chatarea")
                            //var sendMessage = document.getElementById("send")
                            setInterval(function(){
                                var myRequest = new XMLHttpRequest()
                            myRequest.open('POST', 'conversation.php')
                            
                            myRequest.onload = function() {
                                var myData = myRequest.responseText;
                                chatArea.innerHTML = myData;
                                 
                                
                            }
                            myRequest.send()
                            
                            
                            },1000)
                            

                            document.getElementById("send").addEventListener("click", function(e){
                                e.preventDefault();
                                var myRequest = new XMLHttpRequest()
                                myRequest.open('POST', 'sendmessage.php')
                            <?php
                            echo "var toUser='$toUser';";
                            echo "var fromUser = '$fromUser';";
                            //echo "var message='$message';";
                            echo 'var message=document.getElementById("sendtextarea").value;';
                            ?>
                            myRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                            myRequest.onload = function() {
                                var myData = myRequest.responseText;
                                //console.log(myData);
                                setTimeout(() => {
                                    chatArea.scrollIntoView(false) 
                                }, 1000);    
                            }   
                        
                            myRequest.send("user="+toUser+"&sender="+fromUser+"&message="+message)
                            document.getElementById("sendtextarea").value=""
                            document.getElementById("sendtextarea").focus()
                        //myRequest.send("user=sherwinidol")   
                            
                            myRequest.open('POST', 'conversation.php')
                            myRequest.onload = function() {
                                var myData = myRequest.responseText;
                                chatArea.innerHTML = myData;
                                setTimeout(() => {
                                    chatArea.scrollIntoView(false) 
                                }, 1000); 
                                
                            }
                            myRequest.send()
                        
                        })
                        

    
                         </script>
                    </div>
                </form>
            </div>
        <div class="col-md-4 col-sm-4 col-xs-12"></div>
    </div>
    </div>     
<div class="mt-4">

    
</div>
<script src="main.js"></script>
</body>
</html>