<?php
session_start();
include("links.php");
include("functions.php");
include("dbconnection.php");

if (isset($_POST['log-in'])) {
    
    $uname = mysqli_real_escape_string($conn,$_POST['uname']);
    //$uname = $_POST['uname'];
    $password = mysqli_real_escape_string($conn,$_POST['password']);
    //$password = $_POST['password'];
    if (count(explode(" ",$uname))>1) {
        echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";   
        if (count(explode(" ",$password))>1) {
            echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";   
        }
    } elseif (count(explode(" ",$password))>1) {
        echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";   
    } elseif (login($conn, $uname, $password) == true){
        $_SESSION['username'] = $uname;
        $_SESSION['password'] = $password;
        header("location:index.php");
    }
    else {
        echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";
    }
    





    
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<script>
        window.addEventListener("load", () =>document.getElementById('name').focus())
    </script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css" />
    <title>Log-in</title>
</head>
<body class="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12 mt-4" id="shadow" style="min-width:350px; max-width:480px">
                <div class="mt-4 mb-4">
                    <form action="" method="POST" class="" >
                        <h1 class="text-center">Log-in</h1>
                        <input type="text" class="form-control mt-4" id="uname" name="uname" placeholder="Username" />
                        <input type="password" class="form-control mt-1" name="password" placeholder="Password" />
                        <div class="mt-4">
                            <button class="btn btn-success w-100" id="log-in" name="log-in">Log-in</button>
                        </div>
                    </form>
                    <div class="text-center">
                    <p class="mb-0">Not registered yet?<a href="sign-up.php">Sign-up here</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
        </div>
    </div>
</body>
</html> 