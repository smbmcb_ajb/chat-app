<?php


function invalidUname($uname) {
    $result;
    if (preg_match(("/^[a-zA-Z0-9]*$/"), $uname)) {
        $result = true;
    }
    else {
        $result = false;
    }
    return $result;
}
function passwordMatch($passwordRepeat,$password) {
    $passwordMatch;
    if ($passwordRepeat !== $password) {
        $passwordMatch = false;
    }
    else {
        $passwordMatch = true;
    }
    return $passwordMatch;
}
function userNameExist($conn, $uname) {
    $sql = "SELECT * FROM users WHERE user = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s",$uname);
    mysqli_stmt_execute($stmt);

    $resultDatas = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_num_rows($resultDatas) > 0) {
        $resultque = true;
    }
    else {
        $resultque = false;
        return $resultque;
    }
    mysqli_stmt_close($stmt);
}

function addUser($conn, $uname, $password) {
    $sql = "INSERT INTO users(user,userspassword)VALUES(?,?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    if (!mysqli_stmt_bind_param($stmt, "ss",$uname, $password)) {
        
    }
    if (!mysqli_stmt_execute($stmt)) {
        
        die("Error creating new user" . mysqli_connect_error());
    } else {
        echo "<div class='alert alert-success text-center'>New User created successfuly! You may now proceed to log-in page. </div>";        
        
    }

    

    
}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function login($conn, $uname, $password) {
    $sql = "SELECT * FROM users WHERE user = ? && userspassword = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ss",$uname,$password);
    mysqli_stmt_execute($stmt);

    $resultDatas = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_num_rows($resultDatas) > 0) {
        $resultque = true;
    }
    else {
        $resultque = false;
        
        //echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";
    }
    return $resultque;
    mysqli_stmt_close($stmt);
}

function searchUser($conn, $searchKey) {
    $sql = "SELECT user FROM users WHERE UPPER(user) LIKE UPPER(?) ORDER BY user;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    
    mysqli_stmt_bind_param($stmt, "s",$searchKey);
    mysqli_stmt_execute($stmt);

    $resultDatas = mysqli_stmt_get_result($stmt);

    if (mysqli_num_rows($resultDatas) > 0) {
        $resultque = true;
        
        while($row=mysqli_fetch_assoc($resultDatas)) {
            if ($row['user'] <> $_SESSION['username']) {
           echo '<li><h4><a href="talkroom-friend.php?friend='.$row["user"].'">'.$row["user"].'</a></h4></li>';
        }}
        
    }
    else {
        $resultque = false;
        
        //echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";
    }
    return $resultque;
    mysqli_stmt_close($stmt);
}

function sendMessage($conn, $fromUser, $toUser, $message) {
    $sql = "INSERT INTO messages(fromUser,toUser, messageConvo)VALUES(?,?,?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    if (!mysqli_stmt_bind_param($stmt, "sss",$fromUser, $toUser, $message)) {
        
    }
    if (!mysqli_stmt_execute($stmt)) {
        
        die("Error creating new user" . mysqli_connect_error());
    } 
}

function searchConvo($conn, $toUser,$fromUser) {
    $sql = "SELECT * FROM messages WHERE (toUser = ? && fromUser = ?) OR (fromUser = ? && toUser = ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ssss",$toUser,$fromUser,$toUser,$fromUser);
    mysqli_stmt_execute($stmt);

    $resultDatas = mysqli_stmt_get_result($stmt);

    if (mysqli_num_rows($resultDatas) > 0) {
        $resultque = true;
        
        while($row=mysqli_fetch_assoc($resultDatas)) {
            if (($row['fromUser']==$fromUser)&&($row['toUser']==$toUser)) {
                echo '<div class="right-message"><p>'.$row['messageConvo'].'</p></div>';
            }
            if (($row['toUser']==$fromUser)&&($row['fromUser']==$toUser)){
                echo '<div class="left-message"><p>'.$row['messageConvo'].'</p></div>';
            }
                
         }
        
    }
    else {
        $resultque = false;
        
        //echo "<div class='alert alert-danger text-center'>Unable to Log-in. Credentials not found!</div>";
    }
    return $resultque;
    mysqli_stmt_close($stmt);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    
</head>
<body>

</body>
</html>